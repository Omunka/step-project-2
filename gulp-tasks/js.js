const gulp = require('gulp');
const minifyjs = require('gulp-js-minify');
// const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
 
function handleJS(){
    return gulp
        .src("src/js/*.js")
        // .pipe(uglify()) 
        .pipe(minifyjs())
        .pipe(concat("scripts.min.js"))
        .pipe(gulp.dest("build/js"));
     
};

exports.js = handleJS;

